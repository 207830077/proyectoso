# Guia de Instalacion de Servidor OpenVPN y Servicio DNS

## Pasos para la instalacion y configuracion del servicio OPENVPN
    Primeramente y lo recoemndado realizar antes de una instalacion sudo apt update
    installar openvpn con el siguiente comando sudo apt install openvpn

## CREACION DE CA
    agregar el repositorio de EASYRSA wget -P ~/ https://github.com/OpenVPN/easy-rsa/releases/download/v3.0.4/EasyRSA-3.0.4.tgz
    extraer EASYRSA tar xvf EasyRSA-3.0.4.tgz
    Pararnos oDentro de este directorio, hay un archivo llamado vars.example. Haga una copia de este archivo y asigne a esta el nombre vars sin agregar una extensión: dirigirnos a EASYRSA con el comando cd ~/EasyRSA-3.0.4/
    cp vars.example vars asi como tambien abrir con nano este archivo con nano vars
    Buscar las siguientes lineas:
        #set_var EASYRSA_REQ_COUNTRY    "US"
        #set_var EASYRSA_REQ_PROVINCE   "California"
        #set_var EASYRSA_REQ_CITY       "San Francisco"
        #set_var EASYRSA_REQ_ORG        "Copyleft Certificate Co"
        #set_var EASYRSA_REQ_EMAIL      "me@example.net"
        #set_var EASYRSA_REQ_OU         "My Organizational Unit"
    Descomentar todas estas lineas quitandoles el # al inicio
    Reemplace la informacion con su informacion
    Dentro del directorio EasyRSA hay una secuencia de comandos llamada ​​​​easyrsa, que se usar para llevar a cabo varias tareas relacionadas con la creación y administración de la CA. Ejecute la secuencia de comandos con la opción init-pki para iniciar la infraestructura de clave pública en el servidor de CA
    Genere la CA con easyrsa facilmente con el siguiente comando:
    ./easyrsa init-pki
    Seguidamente ingrese el siguiente comando:
    ./easyrsa build-ca nopass el NOPASS es apra que la genere sin password, para que no hayan problemas luego de conexion

### Crear los archivos de certificado, clave y cifrado del servidor
    generar un archivo req que despues firmaremos con nuestra ca ./easyrsa gen-req server nopass
    copiamos la llave de server al directorio de openvpn: sudo cp ~/EasyRSA-3.0.4/pki/private/server.key /etc/openvpn/
    firmamos el req del server con nuestra ca: ./easyrsa sign-req server server
    Nos preguntara si queremos firmarla digitamos: yes
    copiamos el server.crt y nuestra ca a openvpn:  sudo cp /tmp/{server.crt,ca.crt} /etc/openvpn/
    cd EasyRSA-3.0.4/
    Desde ahí, cree una clave segura Diffie-Hellman para usarla durante el intercambio de claves escribiendo:
    ./easyrsa gen-dh
    Esta operación puede tardar unos minutos. Una vez que se complete, genere una firma HMAC para fortalecer las capacidades de verificación de integridad TLS del servidor:
    sudo openvpn --genkey --secret ta.key
    Seguidamente copiaremos estos archivos creamos en el directorio /etc/openvpn/ para utilizarlos en la configuracion
    sudo cp ~/EasyRSA-3.0.4/ta.key /etc/openvpn/
    sudo cp ~/EasyRSA-3.0.4/pki/dh.pem /etc/openvpn/

### Generar un par de certificado y clave de cliente
    mkdir -p ~/client-configs/keys crear directorio donde se guardaran las llaves de los clientes
    chmod -R 700 ~/client-configs  asignamos permisos a este archivo
    Dirigimos al EASYRSA Y creamos el req de nuestro cliente WINDOWS
    cd ~/EasyRSA-3.0.4/
    ./easyrsa gen-req windows nopass
    cp pki/private/windows.key ~/client-configs/keys/ Copiamos la llave de este cliente a nuestro directorio de llaves para clientes
    ./easyrsa sign-req client windows : Firmamos el req de este cliente pero de modo client
    Seguidamente digitamos: yes
    cp /pki/issued/windows.crt ~/client-configs/keys/ copiamos nuestro crt del cliente windows a nuesto client keys
    sudo cp ~/EasyRSA-3.0.4/ta.key ~/client-configs/keys/
    sudo cp /etc/openvpn/ca.crt ~/client-configs/keys/ 
    Copiamos nuestra ca.crt y nuestra ta.key al directorio de clients key

## Configurar el servicio de OpenVPN 
    Comience copiando un archivo de configuración de OpenVPN de muestra al directorio de configuración y luego extráigalo para usarlo como base para su configuración:
    sudo cp /usr/share/doc/openvpn/examples/sample-config-files/server.conf.gz /etc/openvpn/
    sudo gzip -d /etc/openvpn/server.conf.gz
    Abra el archivo de configuración del servidor en su editor de texto preferido:
    sudo nano /etc/openvpn/server.conf
    Debajo, agregue una directiva auth para seleccionar el algoritmo de codificación de mensajes HMAC. SHA256 es una buena opción: auth SHA256
    dh dh.pem
    user nobody
    group nogroup
## Aplicar cambios de DNS para redireccionar todo el tráfico a través de la VPN
    Descomentar las siguientes lineas
    push "redirect-gateway def1 bypass-dhcp"
    push "dhcp-option DNS 208.67.222.222"
    push "dhcp-option DNS 208.67.220.220"
    Luego 
    nano /etc/sysctl.conf
    net.ipv4.ip_forward=1
    sysctl -p
    nano /etc/ufw/before.rules
    Ingresar las siguientes linea a este archivo:
    # START OPENVPN RULES
    # NAT table rules
    *nat
    :POSTROUTING ACCEPT [0:0] 
    # Allow traffic from OpenVPN client to eth0 (change to the interface you discovered!)
    -A POSTROUTING -s 10.8.0.0/8 -o eth0 -j MASQUERADE
    COMMIT
    # END OPENVPN RULES
    Seguidamente: nano /etc/default/ufw
    DEFAULT_FORWARD_POLICY="ACCEPT"

 ## FIREWALL   
    apt-get install ufw
    sudo ufw allow 1194/udp
    sudo ufw allow OpenSSH
    sudo ufw disable
    sudo ufw enable

###  Iniciar y habilitar el servicio de OpenVPN
    sudo systemctl start openvpn@server
    sudo systemctl status openvpn@server
    sudo systemctl enable openvpn@server: habilítelo para que se cargue de manera automática en el inicio
### Crear la infraestructura de configuración de clientes
    mkdir -p ~/client-configs/files
    cp /usr/share/doc/openvpn/examples/sample-config-files/client.conf ~/client-configs/base.conf
    nano ~/client-configs/base.conf
    remote your_server_ip 1194 : en your_server iria tu direccion publica de donde se hospeda el servicio de openvpn seguidamente del puerto a utilizar que en este caso seria 1194
    user nobody : descomentar esta linea
    group nogroup : descomentar esta linea
    #ca ca.crt
    #cert client.crt
    #key client.key
    #tls-auth ta.key 1 
    Descomentar las lineas mostradas anteriormente
    cipher AES-256-CBC
    auth SHA256 : agregar esta linea debajo de Ccypher
    key-direction 1 : agregar tambien

    Seguidamente:
    nano ~/client-configs/make_config.sh
    Agregar este codigo:
                        KEY_DIR=/home/sammy/client-configs/keys
                        OUTPUT_DIR=/home/sammy/client-configs/files
                        BASE_CONFIG=/home/sammy/client-configs/base.conf

                        cat ${BASE_CONFIG} \
                            <(echo -e '<ca>') \
                            ${KEY_DIR}/ca.crt \
                            <(echo -e '</ca>\n<cert>') \
                            ${KEY_DIR}/${1}.crt \
                            <(echo -e '</cert>\n<key>') \
                            ${KEY_DIR}/${1}.key \
                            <(echo -e '</key>\n<tls-auth>') \
                            ${KEY_DIR}/ta.key \
                            <(echo -e '</tls-auth>') \
                            > ${OUTPUT_DIR}/${1}.ovpn
    Donde dice sammy tu deberias poner el nombre de tu computadora el (no root)
    chmod 700 ~/client-configs/make_config.sh : REALIZAR ESTE Archivo como ejecutable
### Generar las configuraciones de clientes
    cd ~/client-configs
    sudo ./make_config.sh windows : en este guia se realiza con el cliente windows. pero se crearon tres servidores mas que son android y linux
    ls ~/client-configs/files : en esta carpeta por el comando anterior se generara un archivo .ovn de cliente el cual utilizaremos para conectarnos despues al openvn server
## Conexion de cliente Linux
### Pasos a seguir
    Descargar Openvpn con el comando apt-get install openvpn
    Descargar el archivo .ovnp de su cliente a conectar
![aa](ovpnO.png)
    Realizar el comando openvpn --config android.ovpn
![Sin titulo](conection3.png)
    Registrar el dns en su maquina linux con el comando nano /etc/resolv.conf
![Sin titulo](configuresolv.png)
    Seguidamente hacer ping al dominio que generamos en nuestro servicio DNS
![Sin titulo](ping.png)
     Comprobar si el dns nos resuelve la busqueda por nombre y nos redirige a nuestra pagina
![Sin titulo](conection2.png)




