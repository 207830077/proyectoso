# Instalación y Configuración del Servicio DNS
    En nuestro caso implementamos el servicio bind9, juntos al servidor de openvpn.

## Pasos para la instalacion y configuracion de DNS BIND9
    sudo apt update 
    sudo apt install -y bind9 dnsutils : instalacion de bind9 y                                    utilidades

### Firewall para nuestro dns activar puertos
    sudo ufw allow dns
    Para configurar el servicio DNS Bind en Debian 9 debemos trabajar sobre los archivos del directorio /etc/bind/. El archivo principal named.conf sólo carga la configuración de otros archivos de configuración adicionales, que son:

        named.conf.options
        named.conf.local
        named.conf.default-zones

    #nameserver 192.168.0.1
    nameserver 10.1.2.5 : ip privada de donde se hospeda el dns
    sudo nano /etc/bind/named.conf.options
        options {
            directory "/var/cache/bind";
            recursion yes;
        }
    agregar el recursion yes
    Agregar forwarders
        forwarders {
                8.8.8.8;
                8.8.4.4;
        };
    sudo named-checkconf : validar si neustra configuracion esta corresta.
    sudo systemctl start bind9 : Iniciar nuestro servidor
    ~$ sudo nano /etc/bind/named.conf.local
### Agregar zonas y zonas inversas
![Sin titulo](intranet.PNG)
![Sin titulo](inversa.PNG)

Seguidamente estableceremos nuestro dns en nuestra maquina:

    nano /etc/resolv.conf
![Sin titulo](resolv.PNG)

