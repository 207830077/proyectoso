
# Instalación y Configuración del Servidor Apache 2

## Paso 1:

Asegúrese de que los repositorios del sistema estén actualizados

    apt-get update
    apt-get upgrade

## Paso 2:

Para instalar el servidor web Apache, ejecute lo siguiente

    apt-get install apache2

## Paso 3:

Una vez completada la instalación, debe habilitar Apache para que se inicie automáticamente al reiniciar el servidor

    systemctl habilita apache2

## Paso 4:

También puede verificar el estado de su servicio Apache

    systemctl status apache2

## Paso 5:

Antes de configurar los hosts virtuales, necesitaremos crear los directorios raíz de documentos para nuestros sitios web. Vamos a crearlos en el directorio / var / www / html

    mkdir -p /var/www/html/domain1.com
    mkdir -p /var/www/html/domain2.com

## Paso 6:

Creemos una página de prueba para cada uno de nuestros dominios, para que luego podamos probar nuestra configuración. Navegue al directorio raíz del documento domain1.com 

    cd /var/www/html/domain1.com

## Paso 7:

Cree una nueva página index.html 

    nano index.html

## Paso 8:

Agregar el siguiente contenido

    <html>
    <cuerpo>
    <center> <h1> ¡Esto es domain1.com! </h1> </center>
    </body>
    </html>

## Paso 9:

Ahora, hagamos lo siguiente para el dominio domain2.com 

    cd /var/www/html/domain2.com
    nano index.html

## Paso 10:

Agregar el siguiente contenido

    <html>
    <cuerpo>
    <center> <h1> ¡Esto es domain2.com! </h1> </center>
    </body>
    </html>

## Paso 11:

Ahora hemos creado con éxito las páginas de prueba para ambos dominios. Para que nuestro servidor web Apache pueda acceder a estos archivos, también debemos otorgarles los permisos adecuados y configurar el usuario y el grupo en www-data . Actualizamos los permisos a todo el directorio / var / www / html, con el siguiente comando.

    chown -R www-data: / var / www / html

## Paso 12:

Ahora podemos crear nuestros archivos de host virtuales. Los archivos de configuración del host virtual generalmente terminan con la extensión .conf.
Ejecute el siguiente comando para crear el archivo de configuración de host virtual para nuestro primer dominio, domain1.com 

    nano /etc/apache2/sites-available/domain1.com.conf

## Paso 13:

Y agregue el siguiente contenido al archivo

    <VirtualHost *: 80>
    ServerAdmin  admin@domain1.co m
    ServerName domain1.com
    ServerAlias ​​www.dominio1.com
    DocumentRoot /var/www/html/domain1.com

    ErrorLog $ {APACHE_LOG_DIR} /domain1.com_error.log
    CustomLog $ {APACHE_LOG_DIR} /domain2.com_access.log combinado

    </VirtualHost>

## Paso 14:

Ahora, hacemos lo mismo para nuestro segundo nombre de dominio, domain2.com

    nano /etc/apache2/sites-available/domain2.com.conf

## Paso 15:

Y agregue el siguiente código

    <VirtualHost *: 80>

    ServerAdmin  admin@domain2.com
    ServerName domain2.com
    ServerAlias ​​www.dominio2.com
    DocumentRoot /var/www/html/domain2.com

    ErrorLog $ {APACHE_LOG_DIR} /domain2.com_error.log
    CustomLog $ {APACHE_LOG_DIR} /domain2.com_access.log combinado

    </VirtualHost>

## Paso 16:

El siguiente paso sería habilitar los hosts virtuales que acabamos de crear. Puede hacer esto con los siguientes comandos

    a2ensite dominio1.com.conf
    a2ensite dominio2.com.conf

## Paso 17:

Una vez que habilite los hosts virtuales, utilizando cualquiera de los métodos anteriores, deberá reiniciar el servidor web Apache.

    systemctl reiniciar apache2

## Paso 18:

Ingresar la URL al navegador y debera de mostrar la pagina web que creamos anteriormente

    domain1.com

![Sin titulo](1.png)

    domain2.com
    
![Sin titulo](2.png)
