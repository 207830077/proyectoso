
# Instalación y Configuración del Servidor MySQL

## Paso 1:

Actualizar los paquetes de la maquina

    apt-get update

## Paso 2:

Ahora, descargaremos el archivo. 

    wget https://dev.mysql.com/get/mysql-apt-config_0.8.10-1_all.deb

Sino tienes instalado wget, lo puedes descargar con el siguiente comando

    apt-get install wget

## Paso 3:

El archivo debería descargarse en el directorio actual. Enumere los archivos para asegurarse

    ls

Debería poder ver el nombre del archivo que se indica

    mysql-apt-config_0.8.10-1_all.deb

## Paso 4:

Ahora estamos listos para la instalación

    sudo dpkg -i mysql-apt-config*


## Paso 5:

Actualizar nuevamente los paquetes

    sudo apt update

## Paso 6:

Instalar MySQL Server, con el comando

    apt-get install mysql-server

## Paso 7:

Comprobamos si se instalo correctamente mysql. con el comando

    mysqladmin -u root -p version

## Paso 8:

Entrar a MySQL con el siguiente comando, para instalar sakila

mysql -u root -p

## Paso 9:

Instalar sakila con el siguiente comando

wget http://downloads.mysql.com/docs/sakila-db.tar.gz

## Paso 10:

Unas vez intalado, debemos descomprimir el archivo .tar.gz

tar -xzf sakila-db.tar.gz

## Paso 11:

Entrar a sakila

    mysql -u root -p <sakila-schema.sql
    mysql -u root -p <sakila-data.sql

## Paso 12:

Ahora crearemos uno clientes de ejemplo, con los siguientes comandos

    CREATE USER 'usuario'@'%'contaseña'

## PASO 13:

Ver los clientes creados con el siguiente comando

    select User from mysql.user;


