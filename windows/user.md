# Guia de Usuario para clientes windows
    Primeramente, para que sea posible la conexion entre el usuario hacia el servicio del openvpn
    es necesario que el usuario, el cliente tenga un archivo .ovpn, este archivo se genera en la autoridad certificadora 
    firmado por esta tambien, este archivo contiene datos del cliente muy importante como por ejemplo su configuracion, 
    los pasos para que un usuario windows se conecte al servidor openvpn son los siguientes:

## Installar Openvpn GUI en tu Windows

![sin titulo](1.PNG)

## Seguidamente ejcutar este programa (importante este programa no se ejecutara, hay que ir a buscarlo en el panel de notificaciones )
### importar el archi .ovpn mencionado anteriormente 
![sin titulo](import.jpeg)

### Luego de importar tu archivo de cliente, te aparecera un mensaje de importando correctamente, seguidamente busca este cliente en el mismo lugar de donde importaste y selecciona CONECTAR, esto te abrira una ventana que cargara toda tu configuracion y seguidamente si todo esta correcto desaparecera y te dira conectado:
![sin titulo](3.PNG)
![sin titulo](conected.jpeg)

## Finalmente estaras conectado al servidor openvpn como comprobacion puedes hacer ping a los otros servicios que se hospedan en las otras maquinas

    ping 10.1.0.9
    ping 10.1.0.6

    Esto te resolvera el ping habiendo 0 archivos perdidos en el proceso


